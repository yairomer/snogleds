#!/bin/sh
SNOGDIR=$( cd "$( dirname "$0" )/.." && pwd )

PYTHONPATH=$SNOGDIR/Python
export PYTHONPATH

ipython notebook --ipython-dir=$SNOGDIR/ipython_notebook --profile=ipnbserver
