#!/bin/sh
SNOGDIR=$( cd "$( dirname "$0" )/.." && pwd )

PYTHONPATH=$SNOGDIR/Python
export PYTHONPATH

python $SNOGDIR/Python/scripts/start_gpio_server.py
