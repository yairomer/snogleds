class SpidevEmu:
    emulator = True

    def __init__(self):
        pass

    class SpiDev():
        emulator = True

        @classmethod
        def open(cls, x, y):
            pass

        @classmethod
        def writebytes(cls, bytes_list):
            print('Writing to SPI:')
            print(str(bytes_list))

class GPIOEmu:
    emulator = True
    BOARD = None
    OUT = 'out'
    IN = 'in'

    @classmethod
    def setmode(cls, x):
        pass

    @classmethod
    def setup(cls, pin_number, pin_type):
        print('setting pin %d as %s' % (pin_number, pin_type))

    @classmethod
    def output(cls, pin_number, value):
        print('setting pin %d to %s' % (pin_number, value))

    @classmethod
    def input(cls, pin_number):
        print('getting pin %d as True' % pin_number)
        return True
