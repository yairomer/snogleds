import logging

try:
    import RPi.GPIO as GPIO  # Import GPIO library
    import spidev
except Exception, err:
    # Replace real GPIO and SPI with print functions
    print('Could not load hardware packages. Using emulator instead. Error:\n%s\n\n' % err.message)
    from .gpio_emulator import GPIOEmu as GPIO
    from .gpio_emulator import SpidevEmu as spidev

class GPIOController:
    def __init__(self, gpio_pin_out=None, spi_max_speed_hz=None, logger=None):

        self.spi = spidev.SpiDev()
        self.spi.open(0, 1)

        GPIO.setmode(GPIO.BOARD)  # Use board pin numbering

        self.gpio_pin_out = {}
        self.spi_max_speed_hz = None

        if gpio_pin_out is not None:
            self.map_gpio_pin_list(gpio_pin_out)
        if spi_max_speed_hz is not None:
            self.set_spi_max_speed(spi_max_speed_hz)
        if logger is None:
            self.logger = logging.getLogger('gpio_controller')

        self.logger.info('GPIO Controller started')

    @staticmethod
    def is_emulator():
        return 'emulator' in dir(spidev) or 'emulator' in dir(GPIO)

    def set_spi_max_speed(self, spi_max_speed_hz):
        self.spi.max_speed_hz = spi_max_speed_hz
        self.spi_max_speed_hz = spi_max_speed_hz
        self.logger.info('Setting SPI speed to %d',self.spi_max_speed_hz)

    def map_gpio_pin(self, pin_type, pin_name, pin_number):

        if pin_type not in self.gpio_pin_out:
            self.gpio_pin_out[pin_type] = {}
        self.gpio_pin_out[pin_type][pin_name] = pin_number

        if pin_type == 'out':
            GPIO.setup(pin_number, GPIO.OUT)
            self.logger.info('Mapped GPIO pin #%d ("%s") as out pin', pin_number, pin_name)
        elif pin_type == 'in':
            GPIO.setup(pin_number, GPIO.IN)
            self.logger.info('Mapped GPIO pin #%d ("%s") as out pin', pin_number, pin_name)
        else:
            print('Unknown pin type %s' % pin_type )
            raise

    def map_gpio_pin_list(self, gpio_pin_out):
        for pin_type in gpio_pin_out:
            for pin_name, pin_number in gpio_pin_out[pin_type].iteritems():
                self.map_gpio_pin(pin_type, pin_name, pin_number)

    def set_pin(self, pin_name, value):
        pin_number = self.gpio_pin_out['out'][pin_name]
        GPIO.output(pin_number, bool(value))
        self.logger.debug('Set GPIO pin "%s" as %s', pin_name, str(value))

    def get_pin(self, pin_name):
        pin_number = self.gpio_pin_out['in'][pin_name]
        value = GPIO.input(pin_number)
        self.logger.debug('Got GPIO pin "%s" value: %s', pin_name, str(value))
        return value

    def write_to_spi(self, bytes_list):
        self.logger.debug('Writing bytes list to SPI: %s', str(bytes_list))
        self.spi.writebytes(bytes_list)
