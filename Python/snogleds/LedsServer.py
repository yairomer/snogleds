import re
import time
# import colorsys
import threading

import numpy as np

import filesimp
import remoteimp

from .defines import LedsStateTypes
from . import snog_leds_data
from . import config_data as default_config_data

def color_to_bytes_trio(color):
    if type(color) in (list, tuple):
        color = np.array(color)
    elif type(color) is str:
        if color[0] == 'x':
            color = re.findall('[A-Fa-f0-9]{2,2}', color[1:])
            color = np.array([int(x, 16) for x in color])

    return color

class LedsServer:
    def __init__(self,
                 leds_state_file=default_config_data['files']['leds_state_file'],
                 gpio_server_address=default_config_data['gpio_server']['address'],
                 gpio_server_port=default_config_data['gpio_server']['port'],
                 default_leds_state=default_config_data['default_leds_state'],
                 ):

        self.leds_state_file = leds_state_file

        self.leds_state = None
        self.next_leds_state = None

        self.leds_shape = np.array([snog_leds_data['dimensions']['n_leds_x'], snog_leds_data['dimensions']['n_leds_y']])
        self.location_to_serial = np.array(snog_leds_data['mapping']).flatten()
        inds = self.location_to_serial >= 0
        self.n_leds = np.sum(inds)
        if np.min(self.location_to_serial[inds]) != 0:
            raise Exception('Bad led mapping', 'Leds mapping positive indices should start from zero')

        if len(np.unique(self.location_to_serial[inds])) != (np.max(self.location_to_serial[inds])+1):
            raise Exception('Bad led mapping', 'Leds mapping positive indices should cover all numbers up to max_index')

        self.serial_to_location = np.zeros(self.n_leds, np.int)
        self.serial_to_location[self.location_to_serial[inds]] = np.where(inds)[0]

        self.leds_colors_list = [[0, 0, 0]]*self.n_leds

        self.gpio_server_address = str(gpio_server_address)
        self.gpio_server_port = str(gpio_server_port)
        self.gpio = remoteimp.Client(address=self.gpio_server_address, port=self.gpio_server_port)

        self.leds_explicit_state_lock = threading.Lock()
        self.next_leds_state_lock = threading.Lock()
        self.next_leds_state_event = threading.Event()
        self.shutdown_event = threading.Event()

        self.main_led_states_loop = self.main_led_states_loop = self.LedStatesLoopThread(self)
        self.main_led_states_loop.start()

        self.set_led_state(default_leds_state)

    def set_indicator_led(self, newState):
        self.gpio.set_pin('board_led', newState)

    def get_switch1(self):
        return self.gpio.set_pin('switch1')

    def get_switch2(self):
        return self.gpio.set_pin('switch2')

    def get_leds_colors_list(self):
        with self.leds_explicit_state_lock:
            return self.leds_colors_list

    def get_leds_colors_matrix(self):
        with self.leds_explicit_state_lock:
            leds_colors_list = self.leds_colors_list

        return self.list_to_matrix(leds_colors_list).tolist()

    def list_to_matrix(self, leds_colors_list):
        if len(leds_colors_list) > self.n_leds:
            leds_colors_list = leds_colors_list[:self.n_leds]

        leds_colors_list = np.array(leds_colors_list)

        leds_color_matrix = np.zeros([self.leds_shape.prod(), 3], np.int)
        leds_color_matrix[self.serial_to_location[:len(leds_colors_list)]] = leds_colors_list
        leds_color_matrix = leds_color_matrix.reshape(np.r_[self.leds_shape, 3])

        return leds_color_matrix

    def matrix_to_list(self, leds_color_matrix):
        leds_colors_list = np.reshape(leds_color_matrix, [self.leds_shape.prod(), 3])

        leds_colors_list = leds_colors_list[self.serial_to_location]

        leds_colors_list = leds_colors_list.tolist()

        return leds_colors_list

    def set_leds_color_list(self, leds_colors_list):
        self.leds_colors_list[:len(leds_colors_list)] = leds_colors_list
        self.gpio.write_to_spi(np.array(leds_colors_list).flatten().tolist())

    def set_leds_colors_matrix(self, leds_colors_matrix):
        self.set_leds_color_list(self.matrix_to_list(leds_colors_matrix))

    class LedStatesLoopThread(threading.Thread):
        def __init__(self, leds_server):
            threading.Thread.__init__(self)
            self.leds_server = leds_server

        def run(self):
            self.leds_server._run_leds_state_loop()

    def shutdown(self):
        self.shutdown_event.set()
        self.next_leds_state_event.set()

    def set_led_state(self, led_state):
        with self.next_leds_state_lock:
            self.next_leds_state = led_state
        self.next_leds_state_event.set()

    def _run_leds_state_loop(self):
        print('Starting leds state loop')
        self.next_leds_state_event.wait()

        while not self.shutdown_event.is_set():

            with self.next_leds_state_lock:
                self.leds_state = self.next_leds_state
            self.next_leds_state_event.clear()

            filesimp.save(self.leds_state_file, self.leds_state)

            if self.leds_state['leds_state'] == LedsStateTypes.Off:
                print('Setting leds to off')
                leds_colors_list = [[0, 0, 0]]*self.n_leds
                self.set_leds_color_list(leds_colors_list)

                self.next_leds_state_event.wait()

            elif self.leds_state['leds_state'] == LedsStateTypes.SingleColor:
                print('Setting leds to single color: '+str(self.leds_state['color']))
                leds_colors = color_to_bytes_trio(self.leds_state['color'])

                leds_colors_list = leds_colors*self.n_leds

                self.set_leds_color_list(leds_colors_list)

                self.next_leds_state_event.wait()

            elif self.leds_state['leds_state'] == LedsStateTypes.Image:
                print('Setting leds to image: '+str(self.leds_state['image_file']))
                leds_colors_matrix = filesimp.load(self.leds_state['image_file'])

                self.set_leds_colors_matrix(leds_colors_matrix)

                self.next_leds_state_event.wait()

            elif self.leds_state['leds_state'] == LedsStateTypes.Random:
                print('Setting leds random image')
                leds_colors_list = np.random.randint(0, 255, [self.n_leds, 3]).tolist()

                self.set_leds_color_list(leds_colors_list)

                self.next_leds_state_event.wait()

            elif self.leds_state['leds_state'] == LedsStateTypes.RandomDance:
                print('Setting leds random dance image')
                if 'interval' not in self.leds_state:
                    self.leds_state['interval'] = 0.1

                while not self.next_leds_state_event.is_set():
                    leds_colors_list = np.random.randint(0, 255, [self.n_leds, 3]).tolist()
                    self.set_leds_color_list(leds_colors_list)
                    time.sleep(self.leds_state['interval'])

            elif self.leds_state['leds_state'] == LedsStateTypes.FlickerNLeds:
                print('Setting %d leds to flicker between %s & %s' %(self.leds_state['n_leds'],
                                                                     self.leds_state['color1'],
                                                                     self.leds_state['color1']))
                if 'interval' not in self.leds_state:
                    self.leds_state['interval'] = 1

                while not self.next_leds_state_event.is_set():
                    leds_colors_list = [self.leds_state['color1']]*int(self.leds_state['n_leds'])
                    self.set_leds_color_list(leds_colors_list)
                    time.sleep(self.leds_state['interval'])
                    leds_colors_list = [self.leds_state['color2']]*int(self.leds_state['n_leds'])
                    self.set_leds_color_list(leds_colors_list)
                    time.sleep(self.leds_state['interval'])

        print('Leds state loop is shutdown')
