import os

import filesimp

# Load config data
config_data_file = os.path.normpath(os.path.join(os.path.dirname(__file__), '..', '..', 'config.yml'))
if not os.path.isfile(config_data_file):
    with open(config_data_file, 'w') as fid:
        fid.write("""___defaults___: {___load___: .\config.default.yml}\n""")
config_data = filesimp.load(config_data_file)

# Load data bases
databases = {}
for database_name in config_data['databases']:
    databases[database_name] = filesimp.load(config_data['databases'][database_name])

snog_leds_data = databases['snogLedsData']  # exposing default data base

