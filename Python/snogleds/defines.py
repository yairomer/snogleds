class LedsStateTypes(object):
    Off = 'OFF'
    SingleColor = 'SINGLE'
    Image = 'IMAGE'
    Random = 'RANDOM'
    RandomDance = 'RANDOM_DANCE'
    FlickerNLeds = 'FLICKER_N_LEDS'
    Bounce = 'BOUNCE'
