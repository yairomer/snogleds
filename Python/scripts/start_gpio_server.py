import logging

import remoteimp
import raspgpioimp
import snogleds


if __name__ == '__main__':
    gpio_server_port = str(snogleds.config_data['gpio']['server_port'])
    spi_max_speed_hz = snogleds.config_data['gpio']['spi_max_speed_hz']
    gpio_pin_out = snogleds.snog_leds_data['gpio_pin_out']

    logger = logging.getLogger('gpio')
    if 'files' in snogleds.config_data and 'log_file' in snogleds.config_data['files']:
        logger.addHandler(logging.FileHandler(snogleds.config_data['files']['log_file']))
    if 'gpio_log_level' in snogleds.config_data:
        if snogleds.config_data['gpio']['logging_level'] == 'DEBUG':
            logger.setLevel(logging.DEBUG)
        elif snogleds.config_data['gpio']['logging_level'] == 'INFO':
            logger.setLevel(logging.INFO)
        elif snogleds.config_data['gpio']['logging_level'] == 'WARNING':
            logger.setLevel(logging.WARNING)
        elif snogleds.config_data['gpio']['logging_level'] == 'ERROR':
            logger.setLevel(logging.ERROR)
        else:
            raise('Unsupported logging level: "%s"', snogleds.config_data['gpio']['logging_level'])

    gpio = raspgpioimp.GPIOController(gpio_pin_out=gpio_pin_out, spi_max_speed_hz=spi_max_speed_hz, logger=logger)
    gpio_server = remoteimp.Server(gpio, port=gpio_server_port, logger=logger)
    gpio_server.start()
