import sys

from SnogLeds import LedsServer

from flask import Flask
from flask import render_template
# from flask.ext.socketio import SocketIO

ledsServer = None


app = Flask(__name__)
# socketio = SocketIO(app)

@app.route('/')
def indexPage(name=None):
    return render_template('index.html')


# @socketio.on('message')
# def handle_message(message):
    # print('received message: ' + message)

@app.route('/setIndicatorLed/<newStateStr>')
def setIndicatorLed(newStateStr):
    newState = int(newStateStr == 'on')
    ledsServer.setIndicatorLed(newState)
    return ledsServer.lastMessage

@app.route('/setUniformColor/<newColor>')
def setUniformColor(newColor):
    ledsServer.setUniformColor(newColor)
    return ledsServer.lastMessage

@app.route('/setUniformColorHex/<newColorHex>')
def setUniformColorHex(newColorHex):
    ledsServer.setUniformColorHex(newColorHex)
    return ledsServer.lastMessage

@app.route('/setFireVetical')
def setFireVetical():
    ledsServer.setFireVetical()
    return ledsServer.lastMessage



if __name__ == '__main__':
    runOnNetwork                = '--runOnNetwork' in sys.argv

    ledDataFile = sys.argv[1]
    ledsServer = LedsServer(ledDataFile);

    if runOnNetwork:
#        app.debug = True
        # socketio.run(app,host='0.0.0.0', port=80)
        app.run(host='0.0.0.0', port=80,debug=True)
    else:
#        app.debug = True
        # socketio.run(app,host='localhost', port=5000)
        app.run(host='localhost', port=5000,debug=True)

