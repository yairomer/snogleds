import colorsys
import random
import time
import re
import numpy as np


try:
    import RPi.GPIO as GPIO ## Import GPIO library
    import spidev
except:
        pass

class LedController:
    def __init__(self,emulator = False,nLeds = 10):
        self.emulator               = emulator
        self.nLeds                  = nLeds
        
        random.seed(0)

        self.R                      = (int(255),int(  0), int(  0))
        self.G                      = (int(  0),int(255), int(  0))
        self.B                      = (int(  0),int(  0), int(255))
        
        if not self.emulator:
            self.spi                = spidev.SpiDev()
            self.spi.open(0,1)
            self.spi.max_speed_hz   = 100000
            
            GPIO.setmode(GPIO.BOARD) # Use board pin numbering
            GPIO.setup(7, GPIO.OUT)  # Set Led
            GPIO.setup(11, GPIO.IN)  # Set switch 1
            GPIO.setup(15, GPIO.IN)  # Set switch 1
            
    def do(self,action, *args):
        outputText                  = 'something unexpected went wrrong'
        
        if action == 'setNLeds':
            nLeds                   = int(args[0])
            
            self.nLeds             = nLeds         
            outputText = 'Setting number of leds to %d, '%self.nLeds
            
            return outputText
        elif action == 'setLed':
            ledMode                 = args[0]
            if ledMode == 'on':
                outputText          = 'Setting led to on'
                if not self.emulator:
                    GPIO.output(7,True) ## Turn led on
                else:
                    print(outputText)
            elif ledMode == 'off':
                outputText          = 'Setting led to off'
                if not self.emulator:
                    GPIO.output(7,False) ## Turn led on
                else:
                    print(outputText)
            else:
                outputText          = 'Unknown led mode: ' + ledMode
        elif action == 'setColors':
            colorsStr               = args[0]
            colorsStr               = colorsStr[1:] # remove leading x
            if colorsStr[0] == '#':
                colorsStr           = colorsStr[1:]

            colorsHex               = re.findall('[A-Fa-f0-9]{2,2}',colorsStr)
            colors                  = [int(x,16) for x in colorsHex] * self.nLeds
        
            outputText = 'Setting color to %s, '%colors
            
            if not self.emulator:
                self.spi.writebytes( colors  )
            else:
                print(outputText)
            
            return outputText
        elif action == 'setFire':
            vList                   = np.linspace(0,0.1,self.nLeds)
            
            colors                  = (np.array([colorsys.hsv_to_rgb(x,1,1) for x in vList])*255).flatten()
            colors                  = [int(x) for x in colors]
        
            outputText = 'Setting color to %s, '%colors
            
            if not self.emulator:
                self.spi.writebytes( colors  )
            else:
                print(outputText)
            
            return outputText
        else:
            outputText              = 'Unknown led action: ' + action
            
        return outputText

            
            
        