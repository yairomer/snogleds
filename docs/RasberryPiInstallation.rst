Installation process:

- Install raspian

- On first load config: SSH (+ other stuff)

- Install screen:
    - run:
.. code::
        sudo apt-get install screen
        screen

- Get snodleds from repository:
    - run:
.. code::
        sudo mkdir /Data
        sudo chmod 777 /Data
        git clone -v --progress --recursive https://bitbucket.org/yairomer/snogleds.git /Data/snogleds
        sudo chmod -R 777 snogleds/
        sudo chown -R pi:pi /Data/snogleds/

- Set connection startup script:
    - run:
.. code::
        echo "sh /Data/snogleds/Scripts/ssh_login.sh" > ~/.bash_profile

- Install and update relavent packages:
    - run:
        sudo apt-get update
        sudo apt-get upgrade
        sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose python-pip
        pip install pyyaml
        sudo apt-get install samba samba-common-bin 
        sudo apt-get install python-dev python-rpi.gpio

- Set samba share:
    - In /boot/config.txt:
        - Change:
.. code::
            workgroup = $workgroup_name$
            wins support = yes
        - Add:
.. code::
            [Data]
                comment=Data
                path=/Data
                browseable=Yes
                writeable=Yes
                only guest=no
                create mask=0777
                directory mask=0777
                public=yes

- Enable spi (If spi was not enable on first boot then)
    - In /etc/samba/smb.conf:
        - Change:
.. code::
            dtparam=spi=on
        
- reboot

    
    