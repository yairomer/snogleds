import bpy
import numpy as np
import copy
import random
import colorsys

print('Running...')

D = bpy.data

# sceneObj = D.scenes.new('Scene.%d'%len(D.scenes))
# sceneObj.makeCurrent()

sceneObj = bpy.context.scene

# Constants
nx = 15
ny = 10

dx = 0.1
dy = 0.1

ceilingHight = 3

ceilingSize = 7


boardHight = 2.95

boardLength = nx*dx
boardWidth = ny*dy
boardThickness = 0.01


sphereDiameter = 0.08

sphereSegments = 32

wirelengthList = (0.3, 0.35, 0.4, 0.45, 0.5)

cameraLocation = (2.5, 0, 1.5)
cameraYaw = np.pi*0.16

holeRadius = 0.005
wireRadius = 0.003

xVec = (np.arange(nx)-(nx-1)/2) * dx

sinAmp = 0  # boardLength/4
def sinx(x):
    return sinAmp*np.sin(x/boardLength*2*np.pi)


# Clearing data
try:
    bpy.ops.object.mode_set(mode='OBJECT')
except:
    pass

for obj in bpy.data.scenes[0].objects:
    bpy.data.scenes[0].objects.unlink(obj)

for obj in bpy.data.objects:
    if obj.users == 0:
        bpy.data.objects.remove(obj)

for mesh in D.meshes:
    if mesh.users == 0:
        D.meshes.remove(mesh)


# Adding Ceilling
ceilingVerts = [(-ceilingSize/2, -ceilingSize/2, 0),
                (-ceilingSize/2,  ceilingSize/2, 0),
                ( ceilingSize/2, -ceilingSize/2, 0),
                ( ceilingSize/2,  ceilingSize/2, 0),
                ]

ceilingFaces = [(0, 1, 3, 2)]


ceilingMesh = D.meshes.new("ceiling")
ceilingMesh.from_pydata(ceilingVerts, [], ceilingFaces)
ceilingMesh.update(calc_edges=True)

ceilingObj = D.objects.new("ceiling", ceilingMesh)
ceilingObj.location = (0, 0, ceilingHight)

sceneObj.objects.link(ceilingObj)

ceilingObj.hide = True
ceilingObj.hide_select = True


# Adding Board
boardVerts = [(x, sinx(x)-boardWidth/2, -boardThickness/2) for x in np.linspace(-boardLength/2, boardLength/2, 100)] + \
             [(x, sinx(x)+boardWidth/2, -boardThickness/2) for x in np.linspace(-boardLength/2, boardLength/2, 100)] + \
             [(x, sinx(x)-boardWidth/2, +boardThickness/2) for x in np.linspace(-boardLength/2, boardLength/2, 100)] + \
             [(x, sinx(x)+boardWidth/2, +boardThickness/2) for x in np.linspace(-boardLength/2, boardLength/2, 100)]

boardFaces = [(x    , x+  1, x+101, x+100)[::-1] for x in range(99)] + \
             [(x+200, x+201, x+301, x+300)       for x in range(99)] + \
             [(x    , x+  1, x+201, x+200)       for x in range(99)] + \
             [(x+100, x+101, x+301, x+300)[::-1] for x in range(99)] + \
             [(  0,100,300,200)[::-1]] + \
             [( 99,199,399,299)      ]


boardMesh = D.meshes.new("board")
boardMesh.from_pydata(boardVerts, [], boardFaces)
boardMesh.update(calc_edges=True)

boardObj = D.objects.new("board", boardMesh)
boardObj.location = (0, 0, boardHight)

sceneObj.objects.link(boardObj)


# Adding spheres
nVerts = int(sphereSegments*(sphereSegments/2-1)+2)
sphereVerts = [None]*nVerts
for iTheta in range(1,int(sphereSegments/2)):
    for iPhi in range(sphereSegments):
        ind   = (iTheta-1)*sphereSegments + iPhi
        theta = iTheta/(sphereSegments/2)*np.pi
        phi   = iPhi/sphereSegments*2*np.pi

        x = sphereDiameter/2*np.cos(phi)*np.sin(theta)
        y = sphereDiameter/2*np.sin(phi)*np.sin(theta)
        z = sphereDiameter/2*np.cos(theta)

        sphereVerts[ind] = (x, y, z)

sphereVerts[-2] = (0, 0,  sphereDiameter/2)
sphereVerts[-1] = (0, 0, -sphereDiameter/2)


sphereFaces = [None]*int(sphereSegments*(sphereSegments/2))
for iPhi in range(sphereSegments):
    ind = iPhi
    sphereFaces[ind] = (nVerts-2, iPhi, (iPhi+1) % sphereSegments)

for iTheta in range(1, int(sphereSegments/2-1)):
    for iPhi in range(sphereSegments):
        ind = iTheta*sphereSegments+iPhi
        sphereFaces[ind] = ((iTheta-1)*sphereSegments+iPhi                     ,
                            (iTheta-1)*sphereSegments+(iPhi+1) % sphereSegments,
                            iTheta    *sphereSegments+(iPhi+1) % sphereSegments,
                            iTheta    *sphereSegments+iPhi                     ,
                            )

for iPhi in range(sphereSegments):
    ind = int(sphereSegments/2-1)*sphereSegments+iPhi
    sphereFaces[ind] = (int(sphereSegments/2-2)*sphereSegments+iPhi,int(sphereSegments/2-2)*sphereSegments+(iPhi+1)%sphereSegments,nVerts-1)




spheresObjList = [None]*(nx*ny)
for ix in range(nx):
    for iy in range(ny):
        ind = ix*ny+iy

        x = (ix-(nx-1)/2)*dx
        y = (iy-(ny-1)/2)*dy + sinx(x)
        z = boardHight-wirelengthList[0]

        sphereMesh = D.meshes.new('sphere.%03d'%ind)
        sphereMesh.from_pydata(sphereVerts,[],sphereFaces)
        sphereMesh.update(calc_edges=True)

        spheresObjList[ind] = D.objects.new('sphere.%03d'%ind, sphereMesh)

        sceneObj.objects.link(spheresObjList[ind]  )
        spheresObjList[ind].location = (x,y,z)

        sceneObj.objects.active = spheresObjList[ind]
        sphereMaterial = bpy.data.materials.new('visuals')
        sphereMaterial.diffuse_shader = 'LAMBERT'
        sphereMaterial.diffuse_intensity = 0.3
        sphereMaterial.specular_intensity = 0.4
        sphereMaterial.emit=0.2
        bpy.context.object.data.materials.append(sphereMaterial)


# Camera
cameraData = D.cameras.new('ortho')
cameraObj  = D.objects.new('Camera', cameraData)

cameraObj.location = cameraLocation
cameraObj.rotation_mode = 'ZYX'
cameraObj.rotation_euler = (0, np.pi/2+cameraYaw, np.pi/2)

sceneObj.objects.link(cameraObj)

cameraObj.data.lens_unit = 'FOV'
cameraObj.data.angle = 1.22173

# Light
# bpy.ops.object.lamp_add(type='POINT', view_align=False, location=(3, 3, 0))
# bpy.ops.object.lamp_add(type='POINT', view_align=False, location=(-3, -3, 0))



# Add holes
holesObjList = [None]*len(spheresObjList)
for i, spheresObj in enumerate(spheresObjList):
    loc = copy.copy(spheresObj.location)
    loc[2] = boardHight
    bpy.ops.mesh.primitive_cylinder_add(radius=holeRadius, depth=2*boardThickness, location=loc)
    holesObjList[i] = bpy.context.object


for holesObj in holesObjList:
    holesObj.select = True

holesObj = holesObjList[0]
sceneObj.objects.active = holesObj
bpy.ops.object.join()


boardObj.modifiers.new("Boolean",type='BOOLEAN')
boardObj.modifiers["Boolean"].object = holesObj
boardObj.modifiers["Boolean"].operation = 'DIFFERENCE'

sceneObj.objects.active = boardObj

bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Boolean")

meshTmp = holesObj.data
bpy.data.scenes[0].objects.unlink(holesObj)
bpy.data.objects.remove(holesObj)
D.meshes.remove(meshTmp)


# Add wires
wiresObjList = [None]*len(spheresObjList)
for i, spheresObj in enumerate(spheresObjList):
    wireLength = boardHight-spheresObj.location[2]
    loc = copy.copy(spheresObj.location)
    loc[2] += wireLength/2
    bpy.ops.mesh.primitive_cylinder_add(radius=wireRadius, depth=1, location=loc)
    wiresObjList[i] = bpy.context.object
    wiresObjList[i].scale[2] = wireLength


# change sphere's hight
# wireLengthListFull = list(wirelengthList) * int(len(spheresObjList)/len(wirelengthList))
# random.shuffle(wireLengthListFull)
# wireLengthListFull = [round(wireLength*100-random.randint(0,int(round((wirelengthList[1]-wirelengthList[0])*100))-1))/100. for wireLength in wireLengthListFull]

wireLengthListFull = [
 0.43, 0.31, 0.26, 0.34, 0.43, 0.5 , 0.43, 0.27, 0.31, 0.26,
 0.35, 0.44, 0.3 , 0.39, 0.34, 0.28, 0.36, 0.44, 0.38, 0.46,
 0.45, 0.31, 0.31, 0.31, 0.41, 0.42, 0.41, 0.38, 0.42, 0.3 ,
 0.28, 0.5 , 0.49, 0.37, 0.26, 0.31, 0.47, 0.39, 0.48, 0.41,
 0.34, 0.33, 0.4 , 0.47, 0.43, 0.29, 0.38, 0.42, 0.28, 0.27,
 0.29, 0.38, 0.43, 0.43, 0.29, 0.3 , 0.29, 0.47, 0.28, 0.48,
 0.27, 0.45, 0.34, 0.37, 0.34, 0.34, 0.47, 0.33, 0.37, 0.39,
 0.49, 0.46, 0.4 , 0.28, 0.41, 0.31, 0.38, 0.29, 0.47, 0.37,
 0.34, 0.33, 0.35, 0.41, 0.32, 0.38, 0.26, 0.46, 0.39, 0.4 ,
 0.45, 0.31, 0.48, 0.34, 0.3 , 0.43, 0.39, 0.31, 0.28, 0.35,
 0.34, 0.5 , 0.5 , 0.35, 0.45, 0.34, 0.26, 0.5 , 0.43, 0.5 ,
 0.43, 0.38, 0.41, 0.27, 0.34, 0.46, 0.38, 0.27, 0.5 , 0.4 ,
 0.37, 0.44, 0.3 , 0.32, 0.42, 0.46, 0.38, 0.38, 0.26, 0.46,
 0.26, 0.43, 0.49, 0.27, 0.33, 0.43, 0.44, 0.38, 0.46, 0.38,
 0.37, 0.43, 0.5 , 0.5 , 0.5 , 0.49, 0.37, 0.26, 0.4 , 0.46]


for i, (sphereObj, wireObj) in enumerate(zip(spheresObjList, wiresObjList)):
    # wireLength = wirelengthList[0] + random.random()*(wirelengthList[-1]-wirelengthList[0])
    # wireLength = wirelengthList[random.randint(0,len(wirelengthList)-1)]
    wireLength = wireLengthListFull[i]
    sphereObj.location[2] = boardHight - wireLength
    wireObj.location[2] = boardHight - wireLength/2 + sphereDiameter/4
    wireObj.scale[2] = wireLength - sphereDiameter/2

print(wireLengthListFull)

# change sphere's color
for sphereObj in spheresObjList:
    h = (sphereObj.location[0]+0.7)/1.4 * (60/360)
    s = 1
    v = 1
    color = colorsys.hsv_to_rgb(h, s, v)

    sphereObj.data.materials[0].emit = 0.2
    sphereObj.active_material.diffuse_color = color

print('Finished!')